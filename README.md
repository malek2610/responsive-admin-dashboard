# Responsive Admin Dashboard

This is a responsive admin dashboard built using `HTML`, `CSS` and `JavaScript`.

## CSS Learning
1. Avoid web browser to apply its default margin and padding
   ```CSS
   body {
        margin: 0;
        padding: 0;
    }   
   ```
2. Ude the available heigh area of the display
   ```CSS
   .container{
        height: 100vh;
    }
    ```

3. Push the sidebar to the left when screen size decreases
   ```CSS
   .sidebar {
        overflow-y: auto;
    }
    ```

## Links
- [Google Fonts](https://fonts.google.com/)
- [Material Icons Guide](https://developers.google.com/fonts/docs/material_icons)
- [ApexCharts](https://apexcharts.com/)
- [CDN ApexCharts](https://cdnjs.com/libraries/apexcharts)